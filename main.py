from tkinter import *
from playFairGUI import *
from twoSquareGUI import *
from dvorakGUI import *

def playFair():
    PlayFairGUI();

def twoSquare():
    TwoSquareGUI()

def dvorak():
    DvorakGUI();

mainGUI = Tk()
mainGUI.wm_title("Cryptography Project")
mainGUI.geometry("275x50")
btnPlayFair = Button(mainGUI,text = "Play Fair",command=playFair)
btnPlayFair.place(x=10,y=0)
btnTwoSquare = Button(mainGUI,text = "Two Square",command=twoSquare)
btnTwoSquare.place(x=100,y=0)
btnDvorak = Button(mainGUI,text = "Dvorak",command=dvorak)
btnDvorak.place(x=200,y=0)

mainGUI.mainloop()