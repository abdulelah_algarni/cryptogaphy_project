def createPolybius(keyEntry):
    myApha = list('ABCDEFGHIJKLMNOPRSTUVWXYZ')
    myKey = keyEntry
    myKey = myKey.upper()
    myPolySqSet = set()
    polySq = []
    for k in range(len(myKey)):
        if myKey[k] not in myPolySqSet:
            polySq.append(myKey[k])
            myPolySqSet = set(polySq)  # convert my list into a set to make sure there is no duplicated value

    polySq5x5 = polySq
    myAlphaSet = (polySq5x5)
    for i in range(len(myApha)):
        if myApha[i] not in myAlphaSet:
            polySq5x5.append(myApha[i])
            myAlphaSet = set(polySq5x5)

    return polySq5x5


Cols = []
Rows = []


def polybiusToListKey(entryList, myString):
    myString1 = myString.upper()

    ListKey1, ListKey2 = [], []
    for i in range(len(myString1)):
        Letter = myString1[i]
        if entryList.index(Letter) == 0:
            Rows.append(1)  # add key1 to a list Rows
            Cols.append(1)  # add key1 to a list Rows
            print(Rows, Cols)
        elif entryList.index(Letter) == 1:
            Rows.append(1)
            Cols.append(2)
        elif entryList.index(Letter) == 2:
            Rows.append(1)
            Cols.append(3)
        elif entryList.index(Letter) == 3:
            Rows.append(1)
            Cols.append(4)
        elif entryList.index(Letter) == 4:
            Rows.append(1)
            Cols.append(5)
            #######first line end
        elif entryList.index(Letter) == 5:
            Rows.append(2)
            Cols.append(1)
        elif entryList.index(Letter) == 6:
            Rows.append(2)
            Cols.append(2)
        elif entryList.index(Letter) == 7:
            Rows.append(2)
            Cols.append(3)
        elif entryList.index(Letter) == 8:
            Rows.append(2)
            Cols.append(4)
        elif entryList.index(Letter) == 9:
            Rows.append(2)
            Cols.append(5)
            #############end 2nd line
        elif entryList.index(Letter) == 10:
            Rows.append(3)
            Cols.append(1)
        elif entryList.index(Letter) == 11:
            Rows.append(3)
            Cols.append(2)
        elif entryList.index(Letter) == 12:
            Rows.append(3)
            Cols.append(3)
        elif entryList.index(Letter) == 13:
            Rows.append(3)
            Cols.append(4)
        elif entryList.index(Letter) == 14:
            Rows.append(3)
            Cols.append(5)
            ###########end 3rd line
        elif entryList.index(Letter) == 15:
            Rows.append(4)
            Cols.append(1)
        elif entryList.index(Letter) == 16:
            Rows.append(4)
            Cols.append(2)
        elif entryList.index(Letter) == 17:
            Rows.append(4)
            Cols.append(3)
        elif entryList.index(Letter) == 18:
            Rows.append(4)
            Cols.append(4)
        elif entryList.index(Letter) == 19:
            Rows.append(4)
            Cols.append(5)
            #####end 4th line
        elif entryList.index(Letter) == 20:
            Rows.append(5)
            Cols.append(1)
        elif entryList.index(Letter) == 21:
            Rows.append(5)
            Cols.append(2)
        elif entryList.index(Letter) == 22:
            Rows.append(5)
            Cols.append(3)
        elif entryList.index(Letter) == 23:
            Rows.append(5)
            Cols.append(4)
        elif entryList.index(Letter) == 24:
            Rows.append(5)
            Cols.append(5)

    return Rows, Cols


def createPolybius(keyEntry1):
    myApha = list('ABCDEFGHIJKLMNOPRSTUVWXYZ')
    myKey1 = keyEntry1
    myKey1 = myKey1.upper()
    myPolySqSet1 = set()
    polySq1 = []
    for k in range(len(myKey1)):
        if myKey1[k] not in myPolySqSet1:
            polySq1.append(myKey1[k])
            myPolySqSet1 = set(polySq1)  # convert my list into a set to make sure there is no duplicated value

    polySq5x5 = polySq1
    myAlphaSet = (polySq5x5)
    for i in range(len(myApha)):
        if myApha[i] not in myAlphaSet:
            polySq5x5.append(myApha[i])
            myAlphaSet = set(polySq5x5)

    return polySq5x5


aCols = []
aRows = []


def polybiusToListKey1(entryList1, myString1):
    myString2 = myString1.upper()

    ListKey3, ListKey4 = [], []
    for i in range(len(myString2)):
        Letter = myString2[i]
        if entryList1.index(Letter) == 0:
            aRows.append(1)  # add key1 to a list Rows
            aCols.append(1)  # add key1 to a list Rows
            print(Rows, Cols)
        elif entryList1.index(Letter) == 1:
            aRows.append(1)
            aCols.append(2)
        elif entryList1.index(Letter) == 2:
            aRows.append(1)
            aCols.append(3)
        elif entryList1.index(Letter) == 3:
            aRows.append(1)
            aCols.append(4)
        elif entryList1.index(Letter) == 4:
            aRows.append(1)
            aCols.append(5)
            #######first line end
        elif entryList1.index(Letter) == 5:
            aRows.append(2)
            aCols.append(1)
        elif entryList1.index(Letter) == 6:
            aRows.append(2)
            aCols.append(2)
        elif entryList1.index(Letter) == 7:
            aRows.append(2)
            aCols.append(3)
        elif entryList1.index(Letter) == 8:
            aRows.append(2)
            aCols.append(4)
        elif entryList1.index(Letter) == 9:
            aRows.append(2)
            aCols.append(5)
            #############end 2nd line
        elif entryList1.index(Letter) == 10:
            aRows.append(3)
            aCols.append(1)
        elif entryList1.index(Letter) == 11:
            aRows.append(3)
            aCols.append(2)
        elif entryList1.index(Letter) == 12:
            aRows.append(3)
            aCols.append(3)
        elif entryList1.index(Letter) == 13:
            aRows.append(3)
            aCols.append(4)
        elif entryList1.index(Letter) == 14:
            aRows.append(3)
            aCols.append(5)
            ###########end 3rd line
        elif entryList1.index(Letter) == 15:
            aRows.append(4)
            aCols.append(1)
        elif entryList1.index(Letter) == 16:
            aRows.append(4)
            aCols.append(2)
        elif entryList1.index(Letter) == 17:
            aRows.append(4)
            aCols.append(3)
        elif entryList1.index(Letter) == 18:
            aRows.append(4)
            aCols.append(4)
        elif entryList1.index(Letter) == 19:
            aRows.append(4)
            aCols.append(5)
            #####end 4th line
        elif entryList1.index(Letter) == 20:
            aRows.append(5)
            aCols.append(1)
        elif entryList1.index(Letter) == 21:
            aRows.append(5)
            aCols.append(2)
        elif entryList1.index(Letter) == 22:
            aRows.append(5)
            aCols.append(3)
        elif entryList1.index(Letter) == 23:
            aRows.append(5)
            aCols.append(4)
        elif entryList1.index(Letter) == 24:
            aRows.append(5)
            aCols.append(5)

    return aRows, aCols


def polybiusToListKey3(entryList, entryList1, myString, myString1):
    myString1 = myString.upper()
    myString2 = myString1.upper()

    ListKey1, ListKey2 = [], []
    ListKey3, ListKey4 = [], []
    for i in range(len(myString1, myString2)):
        Letter = myString1[i]
        Letter = myString2[i]
        if entryList.index(Letter) == 0 and entryList1.index(Letter) == 0:
            aRows.append(1)  # add key1 to a list Rows
            aCols.append(1)  # add key1 to a list Rows
            print(aRows, aCols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 1:
            aRows.append(1)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 2:
            aRows.append(1)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 3:
            aRows.append(1)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 4:
            aRows.append(1)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 5:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 6:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 7:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 8:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 9:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 10:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 11:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 12:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 13:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 14:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 15:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 16:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 17:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 18:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 19:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 20:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 21:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 22:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 23:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 0 and entryList1.index(Letter) == 24:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 0:
            aRows.append(1)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 1:
            aRows.append(1)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 2:
            aRows.append(1)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 3:
            aRows.append(1)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 4:
            aRows.append(1)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 5:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 6:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 7:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 8:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 9:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 10:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 11:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 12:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 13:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 14:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 15:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 16:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 17:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 18:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 19:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 20:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 21:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 22:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 23:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 1 and entryList1.index(Letter) == 24:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 0:
            aRows.append(1)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 1:
            aRows.append(1)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 2:
            aRows.append(1)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 3:
            aRows.append(1)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 4:
            aRows.append(1)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 5:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 6:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 7:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 8:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 9:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 10:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 11:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 12:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 13:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 14:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 15:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 16:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 17:
            Rows.append(1)
            Cols.append(3)
            print(Rows, C3ls)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 18:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 19:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 20:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 21:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 22:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 23:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 2 and entryList1.index(Letter) == 24:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)

        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 0:
            aRows.append(1)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 1:
            aRows.append(1)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 2:
            aRows.append(1)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 3:
            aRows.append(1)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 4:
            aRows.append(1)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 5:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 6:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 7:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 8:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 9:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 10:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 11:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 12:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 13:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 14:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 15:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 16:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 17:
            Rows.append(1)
            Cols.append(3)
            print(Rows, C3ls)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 18:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 19:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 20:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 21:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 22:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 23:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 3 and entryList1.index(Letter) == 24:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 0:
            aRows.append(1)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 1:
            aRows.append(1)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 2:
            aRows.append(1)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 3:
            aRows.append(1)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 4:
            aRows.append(1)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 5:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 6:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 7:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 8:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 9:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 10:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.idex(Letter) == 4 and entryList1.index(Letter) == 11:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 12:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 13:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 14:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 15:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 16:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 17:
            Rows.append(1)
            Cols.append(3)
            print(Rows, C3ls)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 18:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 19:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 20:
            Rows.append(1)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 21:
            Rows.append(1)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 22:
            Rows.append(1)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 23:
            Rows.append(1)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 4 and entryList1.index(Letter) == 24:
            Rows.append(1)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 0:
            Rows.append(2)  # add key1 to a list Rows
            Cols.append(1)  # add key1 to a list Rows
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 1:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 2:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 3:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 4:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 5:
            aRows.append(2)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 6:
            aRows.append(2)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 7:
            aRows.append(2)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 8:
            aRows.append(2)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 9:
            aRows.append(2)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 10:
            aRows.append(2)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 11:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 12:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 13:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 14:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 15:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 16:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 17:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 18:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 19:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 20:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 21:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 22:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 23:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 5 and entryList1.index(Letter) == 24:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 0:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 1:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 2:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 3:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 4:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 5:
            aRows.append(2)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 6:
            aRows.append(2)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 7:
            aRows.append(2)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 8:
            aRows.append(2)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 9:
            aRows.append(2)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 10:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 11:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 12:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 13:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 14:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 15:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 16:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 17:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 18:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 19:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 20:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 21:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 22:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 23:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 6 and entryList1.index(Letter) == 24:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 0:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 1:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 2:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 3:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 4:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 5:
            aRows.append(2)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 6:
            aRows.append(2)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 7:
            aRows.append(2)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 8:
            aRows.append(2)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 9:
            aRows.append(2)
            aCols.aappend(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 10:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 11:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 12:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 13:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 14:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 15:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 16:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 17:
            Rows.append(2)
            Cols.append(3)
            print(Rows, C3ls)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 18:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 19:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 20:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 21:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 22:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 23:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 7 and entryList1.index(Letter) == 24:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)

        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 0:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 1:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 2:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 3:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 4:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 5:
            aRows.append(2)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 6:
            aRows.append(2)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 7:
            aRows.append(2)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 8:
            aRows.append(2)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 9:
            aRows.append(2)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 10:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 11:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 12:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 13:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 14:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 15:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 16:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 17:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 18:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 19:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 20:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 21:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 22:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 23:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 8 and entryList1.index(Letter) == 24:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 0:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 1:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 2:
            Rows.append(2)
            Cols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 3:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 4:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 5:
            aRows.append(2)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 6:
            aRows.append(2)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 7:
            aRows.append(2)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 8:
            aRows.append(2)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 9:
            aRows.append(2)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 10:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.idex(Letter) == 9 and entryList1.index(Letter) == 11:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 12:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 13:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 14:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 15:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 16:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 17:
            Rows.append(2)
            Cols.append(3)
            print(Rows, C3ls)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 18:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 19:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 20:
            Rows.append(2)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 21:
            Rows.append(2)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 22:
            Rows.append(2)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 23:
            Rows.append(2)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 9 and entryList1.index(Letter) == 24:
            Rows.append(2)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 0:
            Rows.append(3)  # add key1 to a list Rows
            Cols.append(1)  # add key1 to a list Rows
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 1:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 2:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 3:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 4:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 5:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 6:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 7:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 8:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 9:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 10:
            aRows.append(3)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 11:
            aRows.append(3)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 12:
            aRows.append(3)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 13:
            aRows.append(3)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 14:
            aRows.append(3)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 15:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 16:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 17:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 18:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 19:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 20:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 21:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 22:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 23:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 10 and entryList1.index(Letter) == 24:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 0:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 1:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 2:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 3:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 4:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 5:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 6:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 7:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 8:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 9:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 10:
            aRows.append(3)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 11:
            aRows.append(3)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 12:
            aRows.append(3)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 13:
            aRows.append(3)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 14:
            aRows.append(3)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 15:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 16:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 17:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 18:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 19:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 20:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 21:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 22:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 23:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 11 and entryList1.index(Letter) == 24:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 0:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 1:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 2:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 3:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 4:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 5:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 6:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 7:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 8:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 9:
            Rows.append(3)
            Cols.aappend(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 10:
            aRows.append(3)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 11:
            aRows.append(3)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 12:
            aRows.append(3)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 13:
            aRows.append(3)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 14:
            aRows.append(3)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 15:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 16:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 17:
            Rows.append(3)
            Cols.append(3)
            print(Rows, C3ls)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 18:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 19:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 20:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 21:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 22:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 23:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 12 and entryList1.index(Letter) == 24:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)

        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 0:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 1:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 2:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 3:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 4:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 5:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 6:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 7:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 8:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 9:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 10:
            aRows.append(3)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 11:
            aRows.append(3)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 12:
            aRows.append(3)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 13:
            aRows.append(3)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 14:
            aRows.append(3)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 15:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 16:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 17:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 18:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 19:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 20:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 21:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 22:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 23:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 13 and entryList1.index(Letter) == 24:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 0:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 1:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 2:
            Rows.append(3)
            Cols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 3:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 4:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 5:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 6:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 7:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 8:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 9:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 10:
            aRows.append(3)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.idex(Letter) == 14 and entryList1.index(Letter) == 11:
            aRows.append(3)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 12:
            aRows.append(3)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 13:
            aRows.append(3)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 14:
            aRows.append(3)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 15:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 16:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 17:
            Rows.append(3)
            Cols.append(3)
            print(Rows, C3ls)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 18:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 19:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 20:
            Rows.append(3)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 21:
            Rows.append(3)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 22:
            Rows.append(3)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 23:
            Rows.append(3)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 14 and entryList1.index(Letter) == 24:
            Rows.append(3)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 0:
            Rows.append(4)  # add key1 to a list Rows
            Cols.append(1)  # add key1 to a list Rows
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 1:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 2:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 3:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 4:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 5:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 6:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 7:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 8:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 9:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 10:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 11:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 12:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 13:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 14:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 15:
            aRows.append(4)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 16:
            aRows.append(4)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 17:
            aRows.append(4)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 18:
            aRows.append(4)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 19:
            aRows.append(4)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 20:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 21:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 22:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 23:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 15 and entryList1.index(Letter) == 24:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 0:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 1:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 2:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 3:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 4:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 5:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 6:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 7:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 8:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 9:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 10:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 11:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 12:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 13:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 14:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 15:
            aRows.append(4)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 16:
            aRows.append(4)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 17:
            aRows.append(4)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 18:
            aRows.append(4)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 19:
            aRows.append(4)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 20:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 21:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 22:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 23:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 16 and entryList1.index(Letter) == 24:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 0:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 1:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 2:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 3:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 4:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 5:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 6:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 7:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 8:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 9:
            Rows.append(4)
            Cols.aappend(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 10:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 11:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 12:
            Rows.append(4)
            Cols.append(3)
            print(Rows, aCols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 13:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 14:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 15:
            aRows.append(4)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 16:
            aRows.append(4)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 17:
            aRows.append(4)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 18:
            aRows.append(4)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 19:
            aRows.append(4)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 20:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 21:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 22:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 23:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 17 and entryList1.index(Letter) == 24:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)

        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 0:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 1:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 2:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 3:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 4:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 5:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 6:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 7:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 8:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 9:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 10:
            aRows.append(4)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 11:
            aRows.append(4)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 12:
            aRows.append(4)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 13:
            aRows.append(4)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 14:
            aRows.append(4)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 15:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 16:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 17:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 18:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 19:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 20:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 21:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 22:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 23:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 18 and entryList1.index(Letter) == 24:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 0:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 1:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 2:
            Rows.append(4)
            Cols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 3:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 4:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 5:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 6:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 7:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 8:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 9:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 10:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.idex(Letter) == 19 and entryList1.index(Letter) == 11:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 12:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 13:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 14:
            aRows.append(4)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 15:
            aRows.append()
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 16:
            aRows.append(4)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 17:
            aRows.append(4)
            aCols.append(3)
            print(aRows, aC3ls)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 18:
            aRows.append(4)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 19:
            aRows.append(4)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 20:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 21:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 22:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 23:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 19 and entryList1.index(Letter) == 24:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 0:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 1:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 2:
            Rows.append(4)
            Cols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 3:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 4:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 5:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 6:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 7:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 8:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 9:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 10:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.idex(Letter) == 20 and entryList1.index(Letter) == 11:
            Rows.append(4)
            Cols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 12:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 13:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 14:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 15:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 16:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 17:
            Rows.append(4)
            Cols.append(3)
            print(Rows, C3ls)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 18:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 19:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 20:
            aRows.append(4)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 21:
            aRows.append(4)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 22:
            aRows.append(4)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 23:
            aRows.append(4)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 20 and entryList1.index(Letter) == 24:
            aRows.append(4)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 0:
            Rows.append(4)  # add key1 to a list Rows
            Cols.append(1)  # add key1 to a list Rows
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 1:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 2:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 3:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 4:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 5:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 6:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 7:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 8:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 9:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 10:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 11:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 12:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 13:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 14:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 15:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 16:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 17:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 18:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 19:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 20:
            aRows.append(4)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 21:
            aRows.append(4)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 22:
            aRows.append(4)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 23:
            aRows.append(4)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 21 and entryList1.index(Letter) == 24:
            aRows.append(4)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 0:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 1:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 2:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 3:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 4:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 5:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 6:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 7:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 8:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 9:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 10:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 11:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 12:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 13:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 14:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 15:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 16:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 17:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 18:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 19:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 20:
            aRows.append(4)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 21:
            aRows.append(4)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 22:
            aRows.append(4)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 23:
            aRows.append(4)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 22 and entryList1.index(Letter) == 24:
            aRows.append(4)
            aCols.append(5)
            print(aRows, aCols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 0:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 1:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 2:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 3:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 4:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 5:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 6:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 7:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 8:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 9:
            Rows.append(4)
            Cols.aappend(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 10:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 11:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 12:
            Rows.append(4)
            Cols.append(3)
            print(Rows, aCols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 13:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 14:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 15:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 16:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 17:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 18:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 19:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 20:
            aRows.append(4)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 21:
            aRows.append(4)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 22:
            aRows.append(4)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 23:
            aRows.append(4)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 23 and entryList1.index(Letter) == 24:
            aRows.append(4)
            aCols.append(5)
            print(aRows, aCols)

        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 0:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 1:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 2:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 3:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 4:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 5:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 6:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 7:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 8:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 9:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 10:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 11:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 12:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 13:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 14:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 15:
            Rows.append(4)
            Cols.append(1)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 16:
            Rows.append(4)
            Cols.append(2)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 17:
            Rows.append(4)
            Cols.append(3)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 18:
            Rows.append(4)
            Cols.append(4)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 19:
            Rows.append(4)
            Cols.append(5)
            print(Rows, Cols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 20:
            aRows.append(4)
            aCols.append(1)
            print(aRows, aCols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 21:
            aRows.append(4)
            aCols.append(2)
            print(aRows, aCols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 22:
            aRows.append(4)
            aCols.append(3)
            print(aRows, aCols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 23:
            aRows.append(4)
            aCols.append(4)
            print(aRows, aCols)
        elif entryList.index(Letter) == 24 and entryList1.index(Letter) == 24:
            aRows.append(4)
            aCols.append(5)
            print(aRows, aCols)

        return Cols, Rows, aCols, aRows


print('Enter First key')
inputkey = input()
polysq = createPolybius(inputkey)
print(polysq)
print('Enter Second key')
inputkey1 = input()
polysq1 = createPolybius(inputkey1)
print(polysq1)
print('Enter message\n')
message = input()
# message = upper(message
message = list(message)
if len(message) % 2 == 1:
    message.append("X")
message = ''.join(message)
print(message)
odd, even = message[::2], message[1::2]
print(odd)
line1, line2 = polybiusToListKey(polysq, odd)
print("Row index  :", line1, '\nColon Index:', line2)
line1, line2 = polybiusToListKey(polysq1, odd)
print("Row index  :", line1, '\nColon Index:', line2)
print(odd)
print(even)
line3, line4 = polybiusToListKey1(polysq1, even)
print("Row index  :", line3, '\nColon Index:', line4)
line3, line4 = polybiusToListKey1(polysq, even)
print("Row index  :", line3, '\nColon Index:', line4)
print(even)

