from tkinter import *
from dvorak import Dvorak


class DvorakGUI():
    def __init__(self):
        self.dv = Tk();
        self.dv.wm_title("Dvorak")
        self.dv.geometry("450x120")

        self.labelMessage = Label(self.dv, text="Message")
        self.labelMessage.place(x=10,y=10)
        self.textMessage = Text(self.dv, width=30,height=1);
        self.textMessage.place(x=125, y=10)

        self.labelResult = Label(self.dv, text="Encryption Result")
        self.labelResult.place(x=10, y=40)
        self.textResult = Text(self.dv,width=30,height=1,state=DISABLED);
        self.textResult.place(x=125, y=40)

        self.btnPlayFair = Button(self.dv, text="Encrypt", command=self.encrypt)
        self.btnPlayFair.place(x=200, y=70)

        self.dv.mainloop()

    def encrypt(self):
        dv = Dvorak()  # create a Playfair object for encrypting and decrypting
        encryptedWord = dv.encrypt(self.textMessage.get())
        self.textResult.delete(0)
        self.textResult.insert(0,encryptedWord)