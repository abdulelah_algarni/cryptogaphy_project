from tkinter import *
from playFair import *

class PlayFairGUI():

    def __init__(self):
        self.pf = Tk();
        self.pf.wm_title("Play Fair")
        self.pf.geometry("450x150")
        self.labelKey = Label(self.pf, text="Key")
        self.labelKey.place(x=10,y=10)
        self.textKey = Text(self.pf,width=30,height=1);
        self.textKey.place(x=125,y=10)

        self.labelMessage = Label(self.pf, text="Message")
        self.labelMessage.place(x=10,y=40)
        self.textMessage = Text(self.pf, width=30,height=1);
        self.textMessage.place(x=125, y=40)

        self.labelResult = Label(self.pf, text="Encryption Result")
        self.labelResult.place(x=10, y=70)
        self.textResult = Text(self.pf,width=30,height=1,state=DISABLED);
        self.textResult.place(x=125, y=70)

        self.btnPlayFair = Button(self.pf, text="Encrypt", command=self.encrypt)
        self.btnPlayFair.place(x=200, y=100)

        self.pf.mainloop()

    def encrypt(self):
        pf = PlayFair(self.textKey.get())  # create a Playfair object for encrypting and decrypting
        encryptedWord = pf.encrypt(self.textMessage.get())
        self.textResult.delete(0)
        self.textResult.insert(0,encryptedWord)