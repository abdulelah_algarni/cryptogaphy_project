import re
class PlayFair(object):
    key = 0;
    def __init__(self,key):
        newKey = ''
        alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

        for letter in key:
            if letter not in newKey and letter in alphabet:
                newKey += letter

        for letter in alphabet:
            if letter not in newKey:
                newKey += letter
        self.key = newKey

    def playfairCipher_pair(self,a, b):
        if a == b:
            b = 'X'
        row, col = int(self.key.index(a) / 5), self.key.index(a) % 5
        row2, col2 = int(self.key.index(b) / 5), self.key.index(b) % 5
        if row == row2:
            return self.key[row * 5 + (col + 1) % 5] + self.key[row2 * 5 + (col2 + 1) % 5]
        elif col == col2:
            return self.key[((row + 1) % 5) * 5 + col] + self.key[((row2 + 1) % 5) * 5 + col2]
        else:
            return self.key[row * 5 + col2] + self.key[row2 * 5 + col]

    def playfairDecipher_pair(self,a, b):
        row, col = int(self.key.index(a) / 5), self.key.index(a) % 5
        row2, col2 = int(self.key.index(b) / 5), self.key.index(b) % 5
        if row == row2:
            return self.key[row * 5 + (col - 1) % 5] + self.key[row2 * 5 + (col2 - 1) % 5]
        elif col == col2:
            return self.key[((row - 1) % 5) * 5 + col] + self.key[((row2 - 1) % 5) * 5 + col2]
        else:
            return self.key[row * 5 + col2] + self.key[row2 * 5 + col]

    def encrypt(self, string):
        string = re.sub(r'[J]', 'I', string)
        if len(string) % 2 == 1:
            string += 'X'
        ret = ''
        for c in range(0, len(string), 2):
            ret += self.playfairCipher_pair(string[c], string[c + 1])
        return ret

    def decrypt(self, string):
        if len(string) % 2 == 1:
            string += 'X'
        ret = ''
        for c in range(0, len(string), 2):
            ret += self.playfairDecipher_pair(string[c], string[c + 1])
        return ret


# if __name__ == '__main__':
#     key = keyMaker("HELOUDC")
#     print(key)
#     encryptedWord = playfairCipher("DISTRICTOFCOLOMBIA")
#     print encryptedWord
#     print(playfairDecipher(encryptedWord))
