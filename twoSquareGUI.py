from tkinter import *

class TwoSquareGUI():
    def __init__(self):
        self.ts = Tk();
        self.ts.wm_title("Two Square")
        self.ts.geometry("450x180")

        self.labelKey1 = Label(self.ts, text="Key 1")
        self.labelKey1.place(x=10,y=10)
        self.textKey1 = Text(self.ts,width=30,height=1);
        self.textKey1.place(x=125,y=10)

        self.labelKey2 = Label(self.ts, text="Key 2")
        self.labelKey2.place(x=10,y=40)
        self.textKey2 = Text(self.ts,width=30,height=1);
        self.textKey2.place(x=125,y=40)

        self.labelMessage = Label(self.ts, text="Message")
        self.labelMessage.place(x=10,y=70)
        self.textMessage = Text(self.ts, width=30,height=1);
        self.textMessage.place(x=125, y=70)

        self.labelResult = Label(self.ts, text="Encryption Result")
        self.labelResult.place(x=10, y=100)
        self.textResult = Text(self.ts,width=30,height=1,state=DISABLED);
        self.textResult.place(x=125, y=100)

        self.btnPlayFair = Button(self.ts, text="Encrypt", command=self.encrypt)
        self.btnPlayFair.place(x=200, y=130)

        self.ts.mainloop()

    def encrypt(self):
        # encryption
        return 0