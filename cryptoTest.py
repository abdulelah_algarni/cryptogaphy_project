
from playFair import PlayFair # import the Playfair library

pf = PlayFair("HELOUDC") # create a Playfair object for encrypting and decrypting
encryptedWord = pf.encrypt("DISTRICTOFCOLOMBIA")
print(encryptedWord)
print(pf.decrypt(encryptedWord))
